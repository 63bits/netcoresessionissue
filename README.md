# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Steps To Perform ###

* Run project in visual studio with CTRL+F5
* Index action in Home controller will set a session value and it will be displayed in view
* Reload browser page few times, you'll see that Session value remains the same
* Without closing a browser or page, go back to visual studio and make any code chage (You can do it as simple as just declare any variable like var x = 5;)
* Save your code
* Go back to browser page (That has been opened before)
* Reload a page
* You'll see session value is updated
