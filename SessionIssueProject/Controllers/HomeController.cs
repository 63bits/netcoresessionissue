﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SessionIssueProject.Controllers
{
    public class HomeController : Controller
    {
        ISession Session;
        public HomeController(IHttpContextAccessor HttpContextAccessor)
        {
            Session = HttpContextAccessor.HttpContext.Session;
        }

        [Route("")]
        public IActionResult Index()
        {
            // Make any code change and session value will be updated

            var SessionValue = Session.GetString("MyValue")?.ToString();
            if (SessionValue == null) 
            {
                SessionValue = $"Value set date is: {DateTime.Now}";
                Session.SetString("MyValue", SessionValue);
            }

            var Model = new IndexModel { SessionValue = SessionValue };

            return View("~/Views/Index.cshtml", Model);
        }
    }

    public class IndexModel
    {
        public string SessionValue { get; set; }
    }
}